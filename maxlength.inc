<?php
/**
 * @file
 *   Business logic for maxlength
 */

function _maxlength_content_form_alter(&$form, &$form_state, $form_id) {
  $type = $form['type']['#value'];
  // update the title as needed
  if (isset($form['title']) && ($values = _maxlength_get_values('title', $type))) {
    $form['title']['#max_length_properties'] = $values;
  }

  // Update the body as needed
  if (isset($form['body_field']['body']) && ($values = _maxlength_get_values('title', $type))) {
    $form['body_field']['body']['#max_length_properties'] = $values;
  }


  if (module_exists('content')) {
    // Get a list of all the CCK fields for this content type
    $info = content_types($type);
    $list = array_keys($info['fields']);

    // Update CCK fields as needed
    foreach ($list as $field) {
      if (isset($form[$field])) {
        foreach (element_children($form[$field]) as $key) {
          // For example #markup doens't have a #type element.
          if (!empty($form[$field][$key]['#type']) && _maxlength_is_supported_widget($form[$field][$key]['#type'])) {
            if ($values = _maxlength_get_values($field, $type)) {
              $form[$field][$key]['#max_length_properties'] = $values;
            }
          }
        }
      }
    }
  }
}

/**
 * Decides if the given widget is supported or not
 *
 * @param
 *   Widget machine-readable name
 * @return
 *   TRUE if supported, FALSE if not
 */
function _maxlength_is_supported_widget($widget) {
  $supported = array(
    'text_textfield',
    'text_textarea',
  );
  return in_array($widget, $supported);
}

function _maxlength_content_type_form_alter(&$form, &$form_state, $form_id) {
  $type = $form['#node_type']->type;
  $labels = array('-3' => 'title', '-1 ' => 'body');

  foreach ($labels as $weight => $label) {

    // bit of a hack to allow us to position the input fields correctly
    $form['submission'][$label .'_label']['#weight'] = $weight - 1;

    $form['submission']['maxlength_' . $label] = array(
      '#type' => 'fieldset',
      '#weight' => $weight,
      '#title' => t('Limit !type  length', array('!type ' => $label)),
      '#collapsible' => TRUE,
      '#collapsed' => strlen(variable_get($type .'_'. $label, '')) == 0,
    );

    $form['submission']['maxlength_' . $label]['maxlength_' . $label] = array(
      '#type' => 'textfield',
      '#title' => t('!label max length', array('!label' => ucwords($label))),
      '#field_suffix' => t('characters'),
      '#return_value' => 1,
      '#size' => 4,
      '#default_value' => variable_get('maxlength_' . $label .'_'. $type, MAXLENGTH_DEFAULT_LENGTH),
      '#description' => t('Maximum number of characters allowed for the !type field of this content type. Leave blank for an unlimited size.', array('!type' => $label)) .'<br/>'.
      '<b>'. t('Please remember, it counts all characters, including HTML, so may not work as expected with rich text editors e.g. FCKeditor / tinyMCE.') .'</b>',
    );
    $form['submission']['maxlength_' . $label]['maxlength_' . $label .'_js'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable remaining characters countdown for the !label', array('!label' => ucwords($label))),
      '#default_value' => variable_get('maxlength_' . $label .'_js_'. $type, MAXLENGTH_DEFAULT_USE_JS),
      '#description' => t('This will enable a Javascript based count down, as well as the client side validation for the !type field of this content type. If no limit set this is ignored.', array('!type' => $label)),
    );
    $form['submission']['maxlength_' . $label]['maxlength_' . $label .'_text'] = array(
      '#type' => 'textarea',
      '#title' => t('!label count down message', array('!label' => ucwords($label))),
      '#default_value' => variable_get('maxlength_' . $label .'_text_'. $type, MAXLENGTH_DEFAULT_TEXT),
      '#description' => t('The text used in the Javascript message under the !type input, where "!limit", "!remaining" and "!count" are replaced by the appropriate numbers.', array('!type' => $label)),
    );
  }
}

function _maxlength_cck_form_alter(&$form, &$form_state, $form_id) {
  $form['field']['maxlength_' . $form['field_name']['#value'] . '_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Maxlength limit'),
    '#description' => t('If you want to use maxlength interactive counter and validation on a CCK field, let\'s leave the normal CCK length unlimited and set this value to the desired one to let maxlength count the real length of the text'),
    '#max_length_properties' => 6,
    '#default_value' => variable_get('maxlength_' . $form['field_name']['#value'], MAXLENGTH_DEFAULT_LENGTH),
  );
  $form['field']['maxlength_' . $form['field_name']['#value'] .'_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable remaining characters countdown for this field'),
    '#default_value' => variable_get('maxlength_' . $form['field_name']['#value'] .'_js', MAXLENGTH_DEFAULT_USE_JS),
    '#description' => t('This will enable a Javascript based count down, as well as the client side validation for this field. If no limit set this is ignored.'),
  );
  $form['field']['maxlength_' . $form['field_name']['#value'] .'_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Count down message'),
    '#default_value' => variable_get('maxlength_' . $form['field_name']['#value'] .'_text', MAXLENGTH_DEFAULT_TEXT),
    '#description' => t('The text used in the Javascript message under the input, where "!limit", "!remaining" and "!count" are replaced by the appropriate numbers.'),
  );
  $form['#submit'][] = '_maxlength_cck_form_submit';
}

function _maxlength_cck_form_submit($form, &$form_state) {
  //note, max lenght for the CCK field is stored in this way as for textareas, its not in $element var passed to theme functions.
  variable_set('maxlength_' . $form['field_name']['#value'], $form['#post']['maxlength_' . $form['field_name']['#value'] .'_length']);
  variable_set('maxlength_' . $form['field_name']['#value'] .'_js', $form['#post']['maxlength_' . $form['field_name']['#value'] .'_js']);
  variable_set('maxlength_' . $form['field_name']['#value'] .'_text', $form['#post']['maxlength_' . $form['field_name']['#value'] .'_text']);
}

function _maxlength_get_values($field = 'body', $type = '') {
  $values = array();
  $values['limit'] = FALSE;

  //CCK
  if (strpos($field, 'field_') === 0) {
    $values['limit'] = variable_get('maxlength_' . $field, MAXLENGTH_DEFAULT_LENGTH);
    $values['use_js'] = variable_get('maxlength_' . $field .'_js', MAXLENGTH_DEFAULT_USE_JS);
    $values['text'] = variable_get('maxlength_' . $field .'_text', MAXLENGTH_DEFAULT_TEXT);
    $values['key'] = 'value';
  } //body and title
  elseif ($type != '') {
    $values['limit'] = variable_get('maxlength_' . $field .'_'. $type, MAXLENGTH_DEFAULT_LENGTH);
    $values['use_js'] = variable_get('maxlength_' . $field .'_js_'. $type, MAXLENGTH_DEFAULT_USE_JS);
    $values['text'] = variable_get('maxlength_' . $field .'_text_'. $type, MAXLENGTH_DEFAULT_TEXT);
  }

  if ($values['limit']) {
    return $values;
  }
  else {
    return FALSE;
  }
}

